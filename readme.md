# Chrome Extentions

> This repository contains multiple chrome extensions, please go to each folder to see the details of each

#### How to enable the Extension:

1. Go to chrome browser
2. open url : chrome://extensions/
3. Click load unpacked extension
4. Select the extension directory : `instagram_layout`
5. A new option will come in taskbar.
