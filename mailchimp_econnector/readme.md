# Mailchimp E-connector
------------------------

### Resources:
* MailChimp’s e-commerce API 3.0 endpoints
*


### Todo
1. Learn and understand how to interact with MailChimp API ( http://developer.mailchimp.com/documentation/mailchimp/guides/get-started-with-mailchimp-api-3/ )  -  8hours

2. Learn e-commerce about resources to use to track store’s carts, orders, products, and customers (https://kb.mailchimp.com/integrations/e-commerce/how-to-use-mailchimp-for-e-commerce?utm_source=mc-api&utm_medium=docs&utm_campaign=apidocs&_ga=2.78448279.2048928035.1508220124-1718308500.1508220124) - 8hours

3. Figure out what needs to store into MailChimp - 16hours

4. Implement functionality for mapping KIBO {entity} informations to MailChimp {entity}. Entity can be order/cart/product etc based on 3rd TODO. - 40hours

5. Create a stand alone application using nodejs(ExpressJS) to do the POC on above above


## Required :  
* Development environment details
