# Change instagram layout

> Instagram Layout brings you the flexibility to change layout of Instagram user profile page,
> which will help to view each image in its maximum width.

> It is an offline application and it stores the information on your computer.
> This plugin uses existing CSS classes available in the Instagram page to do
> the modification, which help to keep the layout persist while loading new
> content in profile page or while switching to other profile page.

* This will be working on
    * profile page
    * Search result page


###### TODO :
1. Icon 128x128
2. Screenshots
    a.  
    b.
    c.
    d.
3. Video
4. Description
5. Publish to chrome stores
