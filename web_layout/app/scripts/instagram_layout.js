const URL_INSTAGRAM = 'https://www.instagram.com/';

const INSTA_LAYOUTS = [
    //Default layout
    [

        {
            selectorText    : `._mesn5`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px auto 30px"'
                },
                {
                    key : 'width',
                    value : '"calc(100% - 40px)"'
                },
                {
                    key : 'padding',
                    value : '"60px 20px 0"'
                }
            ]
        },
        {
            selectorText    : `._mesn5`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"935px"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px auto 30px"'
                },
                {
                    key : 'width',
                    value : '"calc(100% - 40px)"'
                },
                {
                    key : 'padding',
                    value : '"60px 20px 0"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"935px"'
                }
            ]
        },
        {
            selectorText    : `._f2mse`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginRight',
                    value : '"28px"'
                }
            ]
        },
        {
            selectorText    : `._70iju`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginBottom',
                    value : '"28px"'
                },
                {
                    key : 'width',
                    value : '""'
                }
            ]
        },
        {
            selectorText    : `._cmdpi`,
            isAddOrRemove   : 'remove',
            keyValues       : [
                {
                    key : 'flex-direction',
                    value : 'row'
                },
                {
                    key : 'flex-wrap',
                    value : 'wrap'
                }
            ]
        }
    ],
    // 3 column full width layout
    [

        {
            selectorText    : `._mesn5`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"100%"'
                },
                {
                    key : 'padding',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._mesn5`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"100%"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"100%"'
                },
                {
                    key : 'padding',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"100%"'
                }
            ]
        },
        {
            selectorText    : `._f2mse`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginRight',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._70iju`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginBottom',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '""'
                }
            ]
        },
        {
            selectorText    : `._cmdpi`,
            isAddOrRemove   : 'remove',
            keyValues       : [
                {
                    key : 'flex-direction',
                    value : 'row'
                },
                {
                    key : 'flex-wrap',
                    value : 'wrap'
                }
            ]
        }
    ],
    // 6 coulumn layout
    [
        {
            selectorText    : `._mesn5`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"100%"'
                },
                {
                    key : 'padding',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._mesn5`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"100%"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"100%"'
                },
                {
                    key : 'padding',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"100%"'
                }
            ]
        },
        {
            selectorText    : `._f2mse`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginRight',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._70iju`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginBottom',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"50%"'
                }
            ]
        },
        {
            selectorText    : `._cmdpi`,
            isAddOrRemove   : 'add',
            keyValues       : [
                {
                    key : 'flex-direction',
                    value : 'row'
                },
                {
                    key : 'flex-wrap',
                    value : 'wrap'
                }
            ]
        }
    ],
    // 12 coulumn layout
    [
        {
            selectorText    : `._mesn5`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"100%"'
                },
                {
                    key : 'padding',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._mesn5`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"100%"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'margin',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"100%"'
                },
                {
                    key : 'padding',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._jzhdd`,
            keyValues       : [
                {
                    key : 'maxWidth',
                    value : '"100%"'
                }
            ]
        },
        {
            selectorText    : `._f2mse`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginRight',
                    value : '"0px"'
                }
            ]
        },
        {
            selectorText    : `._70iju`,
            conditionText   : '(min-width: 736px)',
            keyValues       : [
                {
                    key : 'marginBottom',
                    value : '"0px"'
                },
                {
                    key : 'width',
                    value : '"25%"'
                }
            ]
        },
        {
            selectorText    : `._cmdpi`,
            isAddOrRemove   : 'add',
            keyValues       : [
                {
                    key : 'flex-direction',
                    value : 'row'
                },
                {
                    key : 'flex-wrap',
                    value : 'wrap'
                }
            ]
        }
    ]
];

var COMMON =  require('./common.js');

/**
 * Matrial component
**/
// import {MDCSlider} from '@material/slider';
// console.log(MDCSlider);

function getRuleFilterScript(classNameStyleMap){
    if(classNameStyleMap.conditionText){
        return `
            if(b.conditionText == '${classNameStyleMap.conditionText}'){
                Array.from(b.cssRules).filter(function(conditonRule){
                    if(conditonRule.selectorText == '${classNameStyleMap.selectorText}'){
                        rule = conditonRule;
                    }
                });
            }
        `;
    }else{
        return `
            if(b.selectorText == '${classNameStyleMap.selectorText}'){
                rule = b;
            }
        `;
    }
}

function getScript(classNameStyleMap){
    let script = '';
    for (var i = 0; i < classNameStyleMap.length; i++) {
        if(classNameStyleMap[i].isAddOrRemove == 'add'){
            let styleMap = classNameStyleMap[i].keyValues.map(function(a){return `${a.key}:${a.value}`;}).join(';')+';';
            console.log(styleMap);
            script = script + `
                document.styleSheets[0].insertRule('${classNameStyleMap[i].selectorText}{${styleMap}}',0);
            `;
        }else if(classNameStyleMap[i].isAddOrRemove == 'remove'){
            script = script + `
                try{
                    Array.from(document.styleSheets[0].cssRules).filter(function(rule){
                        if(rule.selectorText == '${classNameStyleMap[i].selectorText}'){
                            document.styleSheets[0].removeRule('${classNameStyleMap[i].selectorText}');
                        }
                    });
                }catch(error){}
            `;
        }else{
            script = script + `
                var rule = {};
                Array.from(document.styleSheets).filter(function(a){
                    if(a.rules){
                        Array.from(a.rules).filter(function(b){
                            ${getRuleFilterScript(classNameStyleMap[i])}
                        });
                    }
                });
            `
            for(key in classNameStyleMap[i].keyValues){
                script = script + `
                    if(rule && rule.style){
                        rule.style.${classNameStyleMap[i].keyValues[key].key} = ${classNameStyleMap[i].keyValues[key].value};
                    }`;
            }
        }
    }
    return script;

}


function updateLayout(layoutIndex){
    console.log(layoutIndex);
    let script = '',
        scriptGrid = getScript(INSTA_LAYOUTS[layoutIndex]),
        scriptTimeLine = '';

    script = `
            if(document.getElementsByTagName('article').length>1){
                ${scriptTimeLine}
            }else{
                ${scriptGrid}
            }
        `;

    chrome.tabs.executeScript({
      code: script
    });
}


const INSTA_LAYOUT_LABELS = [
    'default',
    'large',
    'medium',
    'small',
    'xsmall'
];
function updateLayoutFeedback(layout){
    document.getElementById('insta_layout').textContent = INSTA_LAYOUT_LABELS[layout];
}

function processInstagramDOM(url, mdc){

    document.getElementById('app_instagram').style.display = 'block';

    // Initialize the slider for change the layout
    var slider = new mdc.slider.MDCSlider(document.querySelector('.mdc-slider'));
    // listen to slider change event
    slider.listen('MDCSlider:change', () => {
        // A Hack to over come the instagram react component to load
        if(slider.value<2){
            updateLayout(0);
            updateLayout(slider.value);
        }else{
            updateLayout(slider.value);
        }

        COMMON.saveLayoutChange(URL_INSTAGRAM,slider.value);
        updateLayoutFeedback(slider.value);
    });

    COMMON.getSavedLayoutChange(URL_INSTAGRAM, (layoutSize) => {
        updateLayout(layoutSize);
        slider.value = layoutSize;
        updateLayoutFeedback(layoutSize);

    });


}


var INSTAGRAM_PAGE = {
    'processInstagramDOM' : processInstagramDOM
};

module.exports = INSTAGRAM_PAGE;
