
/**
 * Sets the given background color for url.
 * @param {string} url URL for which change is to be saved.
 * @param {boolean} change The change action to be saved.
 */
function saveLayoutChange(url, change) {
  var items = {};
  items[url] = change;
  chrome.storage.sync.set(items);
}


function getDocumentWidth(callback){
    var queryInfo = {
      active: true,
      currentWindow: true
    };
    chrome.tabs.query(queryInfo, (tabs) => {
      var tab = tabs[0];
      callback(tab.width);
    });
}

/**
 * Get the current URL.
 * @param {function(string)} callback called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, (tabs) => {
    var tab = tabs[0];
    var url = tab.url;
    console.assert(typeof url == 'string', 'tab.url should be a string');
    callback(url);
  });
}

/**
 * Gets the saved layout status for the URL for url.
 *
 * @param {string} url URL whose background color is to be retrieved.
 * @param {function(string)} callback called with the saved background color for
 *     the given url on success, or a falsy value if no color is retrieved.
 */
function getSavedLayoutChange(url, callback) {
  chrome.storage.sync.get(url, (items) => {
      callback(chrome.runtime.lastError ? null : items[url]);
  });
}


var COMMON = {
    'saveLayoutChange'      : saveLayoutChange,
    'getDocumentWidth'      : getDocumentWidth,
    'getCurrentTabUrl'      : getCurrentTabUrl,
    'getSavedLayoutChange'  : getSavedLayoutChange
};

module.exports = COMMON;
