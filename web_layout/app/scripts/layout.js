/**
 * Importing all the css file required for the application from material frame work
**/
import '../../node_modules/material-components-web/dist/material-components-web.css';

/**
 * Loading material-components-web
**/
var mdc = require('material-components-web');

var BLANK_PAGE =  require('./blank_layout.js');
var INSTAGRAM_PAGE =  require('./instagram_layout.js');
var FACEBOOK_PAGE = require('./facebook_theme.js');
var COMMON =  require('./common.js');

const SUPPORTED_DOMAINS = [
    {//Empty web page
        domain  : 'chrome://',
        func    : function(url){
            BLANK_PAGE.processBlankDOM();
        }
    },
    {// Instagram
        domain  : 'https://www.instagram.com/',
        func    : function(url){
            INSTAGRAM_PAGE.processInstagramDOM(url, mdc);
        }
    },
    {// Facebook
        domain  : 'https://www.facebook.com/',
        func    : function(url){
            FACEBOOK_PAGE.processFacebookDOM(url, mdc);
        }
    }
];
/**
    Main section of the code which will validate if the extension support
    current page or not
    If support this will invoke other functionality
**/
document.addEventListener('DOMContentLoaded', () => {
    COMMON.getCurrentTabUrl((url) => {
        let supportedDomain = SUPPORTED_DOMAINS.find(function(domain_info){
            if(url.indexOf(domain_info.domain) == 0){
                return domain_info;
            }
        });

        if(supportedDomain){
            document.getElementById('error').style.display = 'none';
            supportedDomain.func(url);
        }else {
            document.getElementById('error').style.display = 'block';
        }
    });
});
